#!/usr/bin/env python
# coding: utf-8

# In[481]:


import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import style
import seaborn as sns
import pandas as pd
from pathlib import Path
from datetime import date
from src.models.parameter_estimation.data.data_fetch import get_df, get_hopkins_df
from src.models.parameter_estimation.data.config import link_hopkins
import argparse
import re


# Constants

# In[482]:


arg_parser = argparse.ArgumentParser(description="COVID-19 arg parser", fromfile_prefix_chars="@")
arg_parser.add_argument("-c", "--country", default="Uruguay", help="-c is counrty")
arg_parser.add_argument("-d", "--days", default="2days", help="-d is i.e. 2days")
arg_parser.add_argument("-t", "--kind", default="confirmed", help="-t is the kind of i.e. confirmed/deaths")
arg_parser.add_argument("-o", "--option", nargs='+', default="1", help="-o is the option number to show")
arg_parser.add_argument("-s", "--savepath", default="plot.pgf", help="-s is savepath i.e. plot.pfg")
arg_parser.add_argument("-i", "--display", default=False, help="-i displary instead of saving")

args = arg_parser.parse_args()
OPTIONS = range(1, 28)
DATA_DIR = {"option_{}".format(x): Path("./results_option_{}".format(x)) for x in OPTIONS}
# Number of days we predict into the futurE
PREDICTION_DELTA = [2, 7, 30]


# ### Add function to calculate the loss function

# In[483]:


# TODO


# ### Load Prediction Data

# In[484]:


# Reading the data
pd.set_option("display.width", 120)
result_files = {}
for key, val in DATA_DIR.items():
    result_files[key] = {"{}days".format(days): val.glob("*{}day*.csv".format(days)) for days in PREDICTION_DELTA}

df_pred = pd.DataFrame()
for key, val in result_files.items():
    for days, x_day_prediction in val.items():
        for file in x_day_prediction:
            # date = date.fromisoformat(file.stem[-10:])
            ts = pd.read_csv(file, delimiter=',')
            ts["type"] = days
            ts["option"] = key
            df_pred = df_pred.append(ts)
df_pred = df_pred.rename({'N': 'confirmed', 'Target/Date': 'Date', 'D': 'deaths'}, axis='columns')
df_pred = df_pred[["Country", "Date", "confirmed", "deaths", "type", "option"]]
df_pred['Date'] = pd.to_datetime(df_pred['Date'])
df_pred.set_index(["Date", "Country", "type", "option"], inplace=True)
df_pred


# ### Load Ground Truth Data

# In[485]:


df_hk = {key: pd.read_csv(file) for key, file in link_hopkins.items()}


# ##### Rename death and infected

# In[486]:


for t in [('death', 'deaths'), ('infected', 'confirmed')]:
    df_hk[t[1]] = df_hk.pop(t[0])


# ##### Reshaping the data frame

# In[487]:


dates = df_hk["deaths"].columns[4:]
df_long = {}
for type_, df_x in df_hk.items():
    df_long[type_] = df_x.melt(
        id_vars=['Province/State', 'Country/Region', 'Lat', 'Long'],
        value_vars=dates,
        var_name='Date',
        value_name=type_
    )


# ###### Merging the data frames & Dropping Lat/Long and Renaming Country/Region->Country

# In[488]:


df = df_long["confirmed"].merge(right=df_long["deaths"], on=['Province/State', 'Country/Region', 'Lat', 'Long', 'Date'])
df = df.merge(right=df_long["recovered"], on=['Province/State', 'Country/Region', 'Lat', 'Long', 'Date'])
df['Date'] = pd.to_datetime(df['Date'])
df = df.drop(['Province/State', 'Lat', 'Long'], axis='columns')
df = df.rename({'Country/Region': 'Country'}, axis='columns')


# ##### Check for nan

# In[489]:


df.isna().sum()


# ##### Active Cases

# In[490]:


df['active'] = df['confirmed'] - df['deaths'] - df['recovered']


# In[491]:


df_grouped = df.groupby(['Date', 'Country'])['confirmed', 'deaths', 'recovered', 'active'].sum().reset_index()


# #### Add Cumulative Cases

# In[492]:


# new cases
temp = df_grouped.groupby(['Country', 'Date'])['confirmed', 'deaths', 'recovered']
temp = temp.sum().diff().reset_index()
mask = temp['Country'] != temp['Country'].shift(1)
temp.loc[mask, 'confirmed'] = np.nan
temp.loc[mask, 'deaths'] = np.nan
temp.loc[mask, 'recovered'] = np.nan
# renaming columns
temp.columns = ['Country', 'Date', 'New cases', 'New deaths', 'New recovered']
# merging new values
df_grouped = pd.merge(df_grouped, temp, on=['Country', 'Date'])
# filling na with 0
df_grouped = df_grouped.fillna(0)
# fixing data types
cols = ['New cases', 'New deaths', 'New recovered']
df_grouped[cols] = df_grouped[cols].astype('int')
#
df_grouped['New cases'] = df_grouped['New cases'].apply(lambda x: 0 if x < 0 else x)


# #### Merge Data Frame

# In[493]:


df_pred


# In[494]:


df_gt = pd.DataFrame()
#df_grouped.set_index(["Date", "Country", "type"], inplace=True)
df_gt = df_grouped.set_index(["Date", "Country"])
df_gt['type'] = 'gt'
df_gt['option'] = 'jhk'
df_gt = df_gt.set_index(["type", "option"], append=True)
df_final = pd.merge(df_pred, df_gt, on=['confirmed', 'deaths'], how='outer', right_index=True, left_index=True)
#df_final = df_final.set_index("type", append=True)
print(df_final.index.get_level_values('option').unique())
df_final


# ####  Get the right dates for the ground truth

# In[495]:


print(df_final)


# #### Plot

# In[496]:


# In[480]:


# fig = plt.figure()
# plt.savefig('presentation/figures/crash1.pgf')
COUNTRY = args.country
PRED = args.days
TYPE = args.kind
OPTIONS_TO_PLOT_SIMULTANOUSLY = args.option
SAVE = args.display
SAVE_PATH = args.savepath
#OPTIONS_TO_PLOT_SIMULTANOUSLY = range(9,17)


# In[457]:


OPTIONS = ["option_{}".format(x) for x in OPTIONS_TO_PLOT_SIMULTANOUSLY]
df_final = df_final.sort_index()
preds = []
for OPTION in OPTIONS:
    pred = df_final.loc[pd.IndexSlice[:, COUNTRY, PRED, OPTION], TYPE]
    preds.append(pred)
    print('{} with {}-prediction contains {} obersvations'.format(OPTION, PRED, len(pred)))
    # print(pred)
    if(pred.empty):
        print("Attentin data frame is empty")


# In[458]:


# matplotlib.use("PS")
# matplotlib.rcParams.update({
#     "pgf.texsystem": "pdflatex",
#     'font.family': 'serif',
#     'text.usetex': True,
#     'pgf.rcfonts': False,
# })
sns.set()
sns.set_context("paper")
sns.set_context("poster")
if SAVE:
    plt.rcParams['figure.figsize'] = [16, 9]
    matplotlib.use("pgf")
    matplotlib.rcParams.update({
        "pgf.texsystem": "pdflatex",
        'font.family': 'serif',
        'text.usetex': True,
        'pgf.rcfonts': False,
    })


# In[475]:


fig = plt.figure()
fig.set_figheight(9)
fig.set_figwidth(16)
ax = plt.subplot(111)
label = {"option_{}".format(x): "Model {}".format(x) for x in OPTIONS_TO_PLOT_SIMULTANOUSLY}
label["option_9"] = "Original"
fig.patch.set_facecolor("#FAFAFA")
ax.set_title("{} Day Prediction - {}".format(int(re.findall('\d+', PRED)[0]), COUNTRY), fontname="Helvetica", fontsize=20)
ax.set_facecolor('#FAFAFA')
ax.set_ylabel('['+TYPE+']', fontname="Helvetica", fontsize=20, rotation=0)
ax.yaxis.set_label_coords(-0.01, 1)
ax.set_xlabel("Date", fontname="Helvetica", fontsize=20, labelpad=5)
plt.xticks(rotation=45, fontname="Helvetica", fontsize=15,)

x_min_gt = pd.Timestamp.max
x_max_gt = pd.Timestamp.min
for pred in preds:
    x = pred.index.get_level_values(0)
    option = pred.index.get_level_values(3).unique().values[0]
    plt.plot(x, pred, label=label[option])
    x_min_gt = x_min_gt if x_min_gt < pred.index.get_level_values(0).min() else pred.index.get_level_values(0).min()
    x_max_gt = x_max_gt if x_max_gt > pred.index.get_level_values(0).max() else pred.index.get_level_values(0).max()

gt = df_final.loc[pd.IndexSlice[x_min_gt:x_max_gt, COUNTRY, "gt", "jhk"], TYPE]
x = gt.index.get_level_values(0)
plt.plot(x, gt, color="#eb811b", label="Ground Truth")
plt.legend(prop={'size': 18})

if SAVE:
    plt.savefig(SAVE_PATH)
else:
    plt.show()


# In[ ]:


print(x_min_gt)
print(x_max_gt)
df_final.loc[pd.IndexSlice[x_min_gt:x_max_gt, "gt", "jhk"], TYPE].index.get_level_values(0).unique()


# In[463]:


print(x_min_gt)
print(x_max_gt)
df_final.loc[pd.IndexSlice[:, :, "7days", "option_17"], TYPE].index.get_level_values(0).unique()
