field_list = ["date","positive","hospitalized","recovered","death"]
plot_fields = ["positive","hospitalized","recovered","death"]
url = "https://covidtracking.com/api/states/daily"
filepath = "state_dict"