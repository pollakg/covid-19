import requests
import matplotlib.pyplot as plt
from config import field_list, filepath, plot_fields
import shelve
from data_fetch import get_states, deshelve

def plot_graphs(state_dict, state_list,field_list):
    fig, axs = plt.subplots(7,8, sharex=True, sharey=True)
    i = 0
    for ax in axs.flat:
        plot_data =  state_dict[state_list[i]]["state_data"]
        for field in field_list:
            ax.plot(plot_data[field], label = field)
        ax.set_title(state_list[i], fontsize = 9)
        i += 1
    plt.legend()
    plt.show()

state_list = get_states("states.txt")
state_dict = deshelve()

plot_graphs(state_dict, state_list, plot_fields)