# Covid-19

## Current Model

The model which is currently used to make predictions consists of an LSTM which is fed with 4 features:
- death/infected people (from JHU) 
- government restrictions (https://covidtrackerapi.bsg.ox.ac.uk)
- mobility data (apple)
- temperature data (https://github.com/open-covid-19/data )

In addition to this time series data, we also feed some static data ('Population', 'Density', 'Land Area', 'Med. Age', 'Urban Population (percent)') to the model 

The Code for the model can be found in src/models/parameter_estimation. The model can be executed with 
```python-console
python main_LSTM.py
```
inside main_LSTM.py it is possible to tune some hyperparameters.
## Structure
Their exist two more README:
- src/models/README.md: about the models we use
- src/data/README.md: about the data sources we use

## TODOs
- Georg Richard Pollak
    - Integrate the SIR/SEIR model and add a short example
    - Write a function in the Model class to make predictions conforming with the datathon dataformat
- Victor Steinborn

- Hei Kern Leong
    - Continue the agent based model
    - Maybe add short explanation/links to the src/model/README.md 
- Aurel Schmid
    - Try to convert the measurements data file into a pandas data frame
    - Maybe find more data
- Bernhard Kratzwald
    - Look at fitting parameter for the SIR/SEIR model
    - Add the data sources about regional data
      
#### Further TODOs for anyone to grab
- Look at graph based models/networks see also models README.md
- Go for a beer all together when this is over 
- Change TODOs to Issues
- Find other libraries 
- Find more useful data
- Find useful python notebooks or similar


### How to Run the Code
With ```python main.py --help``` we can see required and optional arguments:
``` python-console
COVID-19 arg parser

optional arguments:
  -h, --help            show this help message and exit
  -m {SIR,SEIR}, --model {SIR,SEIR}
                        specifies model to run with
  -s SAVE_PRED, --save-pred SAVE_PRED
                        specifies whether and where to save the model
  -p, --plot            specifies whether to call plot on the given model
  -t1 START_DATE, --start-date START_DATE
                        Format:
                        dd.mm.yyyy
                        dd.mm
                        days_into_past
                        default=0=TODAY
  -t2 PRED_DATE, --pred-date PRED_DATE
                        Format
                        dd.mm.yyyy
                        dd.mm
                        days_into_future
                        default=30
```
**To run**: i.e.:
``` python
python main.py [-s/--save-pred "my_result_file.csv"] [-p/--plot]
```
or alternatively use a config file of the form:
``` text
--save-pred="my_result_file.csv"
-plot
```
and run with
``` python
python main.py @my_config_file.txt 
```
**Goal**: I want to add options such as N, I0,... that are common for all models.
Also the way you specify models and add arguments to them is WIP and still a bit messy.
Atm: I specify the model still in the file `main.py` using like:

``` python
    args.model = SIR()
```
But I want that you will be able to specify the module and name + args in the config file/the command line

## AIM
### Countries we focused on at the moment
- China
- Italy


