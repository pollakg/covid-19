#!/usr/bin/env python
__authors__ = ["Georg Richard Pollak",
               "Victor Steinborn ",
               "Hei Kern Leong ",
               "Aurel Schmid ",
               "Bernhard Kratzwald"]
__emails__ = ["pollakg@student.ethz.ch",
              "bkratzwald@ethz.ch",
              "aurschmi@student.ethz.ch",
              "hleong@student.ethz.ch",
              "victor.steinborn@mat.ethz.ch"]
__copyright__ = __authors__
__credits__ = []

from src.models.parameter_estimation.LSTM import LSTMModel
from src.models.parameter_estimation.regression import RegModel
import time

time_steps_predict=[7]
epoch_num=6
SAVE_PREDICTION=True
split = 0.75
time_steps_train = 14
loss_func = 'mse'

reg=RegModel()
# t = time.time()
# for time_machine in time_machine_list:
#     for pred in time_steps_predict:
#         model=LSTMModel(pred,epoch_num=epoch_num,SAVE_PREDICTION=SAVE_PREDICTION,split=split,
#                         time_steps_train=time_steps_train,time_machine=time_machine,
#                         monitor=True,loss_func=loss_func)
#         model.run()

# elapsed = time.time() - t
# print ("elapsed time: " + str(elapsed/60) + " mins")


t = time.time()
for pred in time_steps_predict:
    if pred == 2:
        time_machine_list = list(range(3,5))
    elif pred == 7:
        time_machine_list = list(range(8,58))
        # time_machine_list = list(range(8,15))
    elif pred == 30:
        time_machine_list = list(range(31,60))
    for time_machine in time_machine_list:
        model=LSTMModel(pred,epoch_num=epoch_num,SAVE_PREDICTION=SAVE_PREDICTION,split=split,
                        time_steps_train=time_steps_train,time_machine=time_machine,
                        monitor=True,loss_func=loss_func)
        model.run()
elapsed = time.time() - t
print ("elapsed time: " + str(elapsed/60) + " mins")