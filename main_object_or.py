#!/usr/bin/env python
__authors__ = ["Georg Richard Pollak",
               "Victor Steinborn ",
               "Hei Kern Leong ",
               "Aurel Schmid ",
               "Bernhard Kratzwald"]
__emails__ = ["pollakg@student.ethz.ch",
              "bkratzwald@ethz.ch",
              "aurschmi@student.ethz.ch",
              "hleong@student.ethz.ch",
              "victor.steinborn@mat.ethz.ch"]
__copyright__ = __authors__
__credits__ = []
# System Imports
# ------------------------------------------------------------------------------
import argparse
from argparse import RawTextHelpFormatter

# User Imports
# ------------------------------------------------------------------------------
from src.runner import ModelRunner
from src.models.compartemental_models.SIR import SIR
from src.models.compartemental_models.SEIR import SEIR
from src.models.compartemental_models.SIR_N_Day_Avg import SIR_N_Day_Avg
from src.models.parameter_estimation.LSTM import LSTMModel

if __name__ == '__main__':

    arg_parser = argparse.ArgumentParser(description="COVID-19 arg parser",
                                         fromfile_prefix_chars="@",
                                         formatter_class=RawTextHelpFormatter)
    arg_parser.add_argument("-m", "--model",
                            help="specifies model to run with",
                            choices=["SIR", "SEIR", "SIR_N_DAY_AVG","LSTM"],
                            default="SIR")
    arg_parser.add_argument("-s", "--save-pred",
                            help="specifies whether and where to save the model")
    arg_parser.add_argument("-p", "--plot",
                            help="specifies whether to call plot on the given model",
                            action="store_true")
    arg_parser.add_argument("-d1", "--start-date",
                            help="Format:\ndd.mm.yyyy\ndd.mm\ndays_into_past\ndefault=0=TODAY",
                            default="TODAY")
    arg_parser.add_argument("-d2", "--pred_date",
                            help="Format\ndd.mm.yyyy\ndd.mm\ndays_into_future\ndefault=30",
                            default="30"
                            )
    arg_parser.add_argument("-c", "--country",
                            help="Format: country name",
                            default="Switzerland"
                            )

    # ATM how to specify model => has to be improved
    args = arg_parser.parse_args()
    if args.model == "SIR":
        args.model = SIR()
    elif args.model == "SEIR":
        args.model = SEIR()
    # elif args.model == "SIR_N_DAY_AVG":
    #     args.model = SIR_N_Day_Avg(vars(args))
    elif args.model == "SIR_N_DAY_AVG":
        args.model = SIR_N_Day_Avg()
    else:
        args.model = LSTM()

    
    # runner = ModelRunner(vars(args))
    # runner.run()
