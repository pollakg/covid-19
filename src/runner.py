import importlib
import sys
from datetime import date
from datetime import timedelta


class ModelRunner:
    VARIABELS_TO_BE_ESTIMATED = ["province_state",
                                 "country",
                                 "pred_date",
                                 "N",
                                 "low95N",
                                 "high95N",
                                 "R",
                                 "low95R",
                                 "high95R",
                                 "D",
                                 "low95D",
                                 "high95D",
                                 "T",
                                 "low95T",
                                 "high95T",
                                 "M",
                                 "low95M",
                                 "high95M",
                                 "C",
                                 "low95C",
                                 "high95C",
                                 ]
    REQUIRED = ["country", "pred_date", "N", "D"]

    def __init__(self, args):
        """args: namespace"""
        # Load and Parse the Configfiel =======================================
        self.args = args
        self.save_pred = False
        self.plot = False
        keys = args.keys()
        # ============================ The dates ===================
        self.args["start_date"] = self.parse_date(args["start_date"])
        self.args["pred_date"] = self.parse_date(args["pred_date"])
        # ============================ the model ===================
        self.model = args["model"]
        if "plot" in keys:
            self.plot = args["plot"]
        if "save_pred" in keys:
            self.save_pred = args["save_pred"]

        # self.pprint_config()
        # Create model instance ===============================================
        self.model = self._create_model(self.model)

    # Run/excute the model ====================================================
    def run(self, action=None):
        self.model.run()
        self.args.update(self.model.to_be_saved)
        if self.plot:
            if not getattr(self.model, "plot", None):
                sys.exit("Model does not have a plot function!")
            else:
                self.model.plot()
        if self.save_pred:
            self._save_predictions()

    def _create_model(self, model_arg):
        """
            Returns a callable model by either
                - returning a callable directly from the arguments
                - returning a function if the args is a dict&has a function key
                - returning a class if the argument is a dict and has a class key
        """
        if isinstance(model_arg, object):
            model = model_arg
        elif isinstance(model_arg, dict):
            if "module" in model_arg:
                # import module of model_arg
                module = importlib.import_module(model_arg["module"])
                if "class" in model_arg:
                    # module.model_arg.class
                    model = getattr(module, model_arg["class"])()
                elif "function" in model_arg:
                    model = getattr(module, model_arg["function"])
        else:
            sys.exit("Error could not load model")

        return model

    def pprint_config(self):
        pass

    def parse_date(self, d):
        dots = d.count('.')
        today = date.today()
        yyyy, dd, mm = 0, 0, 0
        if dots == 0:
            dd = 0 if d == "TODAY" else d
            # dd
            dd = int(dd)
            if dd == 0:
                return today
            else:
                return today+timedelta(days=int(d))
        elif dots == 1:
            # dd.mm
            dd, mm = d.split(".")
            return date(today.year, int(mm), int(dd))
        elif dots == 2:
            # yyyy.dd.mm
            yyyy, dd, mm = d.split(".")
            return date(int(yyyy), int(mm), int(dd))
        else:
            sys.exit("date format %s is not valid", d)

    def _save_predictions(self):
        """
        Function to save predictions to a csv file args is a dict
        :param province_state: Optional Province/State
        :param country: Country
        :param target_date:  Date in format yyyy-mm-dd
        :param N: total number of cases [d]
        :param D: total number of deaths [d]
        Optional Arguments:
        :param m: mode, how to open file
        :param R: total number of recovered [d]
        :param C: total number of critical cases (hospitalization required) [d]
        :param M: mortality [d]
        :param T: total number of tests [d]
        :param low95N - high95N: 95% confidence interval for N [d]
        :param low95R - high95R: 95% confidence interval for R [d]
        :param low95D - high95D: 95% confidence interval for D [d]
        :param low95T - high95T: 95% confidence interval for T [d]
        :param low95M - high95M: 95% confidence interval for M [d]
        :param low95C - high95C: 95% confidence interval for C [d]
        :returns:
        :rtype:
        """
        prec = 17
        fh = None
        # ================= Check If Required Args are there ==================
        for key in self.REQUIRED:
            if key not in self.args.keys():
                message = key+" missing for prediction"
                sys.exit(message)

        # ================= Open File =======================
        if "save_pred" in self.args.keys():
            mode = self.args["mode"] if "mode" in self.args else "w"
            fh = open(self.args["save_pred"], mode)
        else:
            fh = open('results/results.csv', "w")

        for key in self.VARIABELS_TO_BE_ESTIMATED:
            if key in self.args.keys():
                fh.write(str(self.args[key])+",")
            else:
                fh.write(",")
        fh.write("\n")
        fh.close()
