# Imports
import pandas as pd

# deaths (R)
deaths='https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv'
deathData=pd.read_csv(deaths)
# recovery (R)
recovery='https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv'
recoveryData=pd.read_csv(recovery)
# cases (I)
cases='https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'
casesData=pd.read_csv(cases)

# Use Switzerland as an example (need to find database with total pop.) (todo)
# What c and omega values to use? (todo)
# What output format?

pop = 8544500 # Aprox. population before the start of the outbreak (SWISS-2018)

# From today to tomorow - Very simple method
def sir_daystep(deathData, casesData, recoverData, c, omega):
    # Load most recent data
    deaths=deathData.iloc[:,-1].values[0]
    cases=casesData.iloc[:,-1].values[0]
    recoveries=recoverData.iloc[:,-1].values[0]
    
    # Determine susceptible and remaining pop.
    susceptible = pop - deaths - cases - recoveries
    remainingpop = pop - deaths - recoveries
    
    # SIR model calc.
    delta_S = -c*susceptible* cases / remainingpop
    delta_I = c*susceptible* cases / remainingpop - omega*cases
    delta_R = omega*cases
    
    # Find the next S, I and R values
    next_S = delta_S + susceptible
    next_I = delta_I + cases
    next_R = delta_R + recoveries + deaths
    
    return next_S, next_I, next_R

# Test:
#sir_daystep(swissDeathData, swissCasesData, swissRecoveryData, 0.01, 0.01)

"""
Trying out RK4 Algo. -- More numerically stable
"""
def S_rate_of_change(S,I,N,c):
    result = -c*S*I/N
    return  result

def I_rate_of_change(S,I,N,c,gamma):
    result = c*S*I/N - gamma*I
    return result

def R_rate_of_change(I,gamma):
    result = gamma*I
    return result

def I_rate_of_change_mod(S,I,N,c,gamma, omega): # Modified SIR, takes deaths into account
    result = c*S*I/N - gamma*I -omega*I
    return result

def D_rate_of_change_mod(I, omega): # Modified SIR, takes deaths into account
    result = omega*I
    return result


### Initial Conditions
# Load most recent data
deaths=deathData.iloc[:,-1].values[0]
infections=casesData.iloc[:,-1].values[0]
recoveries=recoveryData.iloc[:,-1].values[0]

# Determine susceptible and remaining pop.
susceptible = pop - deaths - infections - recoveries

def sir_step_RK4(susceptible, infections, recoveries, c, gamma, stepsize):
    # SIR model calc. with RK4 time step
    k1_S = S_rate_of_change(susceptible, infections, pop, c)
    k1_I = I_rate_of_change(susceptible, infections, pop, c, gamma)
    k1_R = R_rate_of_change(             infections, gamma)
    
    k2_S = S_rate_of_change(susceptible+(k1_S/2.), infections+(k1_I/2.), pop, c)
    k2_I = I_rate_of_change(susceptible+(k1_S/2.), infections+(k1_I/2.), pop, c, gamma)
    k2_R = R_rate_of_change(                       infections+(k1_I/2.), gamma)

    k3_S = S_rate_of_change(susceptible+(k2_S/2.), infections+(k2_I/2.), pop, c)
    k3_I = I_rate_of_change(susceptible+(k2_S/2.), infections+(k2_I/2.), pop, c, gamma)
    k3_R = R_rate_of_change(                       infections+(k2_I/2.), gamma)

    k4_S = S_rate_of_change(susceptible+(k3_S), infections+(k3_I), pop, c)
    k4_I = I_rate_of_change(susceptible+(k3_S), infections+(k3_I), pop, c, gamma)
    k4_R = R_rate_of_change(                    infections+(k3_I), gamma)

    delta_S = (stepsize/6.) * (k1_S + 2*k2_S + 2*k3_S + k4_S)
    delta_I = (stepsize/6.) * (k1_I + 2*k2_I + 2*k3_I + k4_I)
    delta_R = (stepsize/6.) * (k1_R + 2*k2_R + 2*k3_R + k4_R)

    # Update predictions
    new_S = susceptible + delta_S
    new_I = infections + delta_I
    new_R = recoveries + delta_R

    return new_S, new_I, new_R

def sir_mod_step_RK4(susceptible, deaths, infections, recoveries, c, gamma, omega, stepsize):
    # SIR model calc. with RK4 time step
    k1_S = S_rate_of_change(    susceptible,infections, pop, c)
    k1_I = I_rate_of_change_mod(susceptible,infections, pop, c, gamma, omega)
    k1_R = R_rate_of_change(                infections, gamma)
    k1_D = D_rate_of_change_mod(            infections, omega)
    
    k2_S = S_rate_of_change(    susceptible+(k1_S/2.),infections+(k1_I/2.), pop, c)
    k2_I = I_rate_of_change_mod(susceptible+(k1_S/2.),infections+(k1_I/2.), pop, c, gamma, omega)
    k2_R = R_rate_of_change(                          infections+(k1_I/2.), gamma)
    k2_D = D_rate_of_change_mod(                      infections+(k1_I/2.), omega)

    k3_S = S_rate_of_change(    susceptible+(k2_S/2.),infections+(k2_I/2.), pop, c)
    k3_I = I_rate_of_change_mod(susceptible+(k2_S/2.),infections+(k2_I/2.), pop, c, gamma, omega)
    k3_R = R_rate_of_change(                          infections+(k2_I/2.), gamma)
    k3_D = D_rate_of_change_mod(                      infections+(k2_I/2.), omega)

    k4_S = S_rate_of_change(susceptible+(k3_S), infections+(k3_I), pop, c)
    k4_I = I_rate_of_change_mod(susceptible+(k3_S), infections+(k3_I), pop, c, gamma, omega)
    k4_R = R_rate_of_change(                    infections+(k3_I), gamma)
    k4_D = D_rate_of_change_mod(                infections+(k3_I), omega)

    delta_S = (stepsize/6.) * (k1_S + 2*k2_S + 2*k3_S + k4_S)
    delta_I = (stepsize/6.) * (k1_I + 2*k2_I + 2*k3_I + k4_I)
    delta_R = (stepsize/6.) * (k1_R + 2*k2_R + 2*k3_R + k4_R)
    delta_D = (stepsize/6.) * (k1_D + 2*k2_D + 2*k3_D + k4_D)

    # Update predictions
    new_S = susceptible + delta_S
    new_I = infections + delta_I
    new_R = recoveries + delta_R
    new_D = deaths + delta_D

    return new_S, new_I, new_R, new_D




