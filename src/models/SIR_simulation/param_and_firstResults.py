# Imports
import pandas as pd
import matplotlib.pyplot as plt
import SIR_Model
from statistics import mean


# Initialize
# deaths (R)
deaths_link='https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv'
deathData=pd.read_csv(deaths_link)
deathData=deathData[deathData['Country/Region']=='Switzerland']
# recovery (R)
recovery_link='https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv'
recoveryData=pd.read_csv(recovery_link)
recoveryData=recoveryData[recoveryData['Country/Region']=='Switzerland']
# cases (I)
cases_link='https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'
casesData=pd.read_csv(cases_link)
casesData=casesData[casesData['Country/Region']=='Switzerland']


pop = 8544500 # Aprox. population before the start of the outbreak (SWISS-2018)

### Initial Conditions
# Load most recent data
deaths_i=deathData.iloc[:,-1].values[0]
infections_i=casesData.iloc[:,-1].values[0]
recoveries_i=recoveryData.iloc[:,-1].values[0]

# Determine susceptible pop.
susceptible_i = pop - deaths_i - infections_i - recoveries_i


## Find params over last two weeks.
# Find Data
deathList=[]
recoveryList=[]
casesList=[]
susceptList=[]
for i in range(15):
    deathList.append(deathData.iloc[:,-(i+1)].values[0])
    casesList.append(casesData.iloc[:,-(i+1)].values[0])
    recoveryList.append(recoveryData.iloc[:,-(i+1)].values[0])
    susceptList.append(pop-deathData.iloc[:,-(i+1)].values[0]
                                            -casesData.iloc[:,-(i+1)].values[0]
                                            -recoveryData.iloc[:,-(i+1)].values[0]
                                            )

# Find rates of change
deltaDeathList=[]
deltaRecoveryList=[]
deltaCasesList=[]
deltaSusceptList=[]
for i in range(14):
    deltaDeathList.append(deathList[i]-deathList[i+1])
    deltaRecoveryList.append(recoveryList[i]-recoveryList[i+1])
    deltaCasesList.append(casesList[i]-casesList[i+1])
    deltaSusceptList.append(susceptList[i]-susceptList[i+1])

# Extract Params.
cList=[]
gammaList=[]
omegaList=[]
for i in range(14):
    cList.append(-deltaSusceptList[i]/(susceptList[i-1]*casesList[i-1]/pop))
    gammaList.append(deltaRecoveryList[i]/susceptList[i-1])
    omegaList.append(deltaDeathList[i]/susceptList[i-1])
    

# Finf Avg. Params.
c_2W=mean(cList)
gamma_2W=mean(gammaList)
omega_2W=mean(omegaList)

# Run simulation
stepsize=1./100
Tot_Steps = int(30/stepsize) # For a 30 day sim.

t=[]
suc=[]
inf=[]
rec=[]
dea=[]
S=susceptible_i
I=infections_i
R=recoveries_i
D=deaths_i

for step in range(Tot_Steps):
    t.append(step*stepsize)
    S,I,R,D = SIR_Model.sir_mod_step_RK4(S, D, I, R, c_2W, gamma_2W, omega_2W, stepsize)
    suc.append(S)
    inf.append(I)
    rec.append(R)
    dea.append(D)


## can plot if desired


