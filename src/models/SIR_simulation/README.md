# SIR Model
---

SIR Model governed by a set of differential equations with one constraint equation

```math
\begin{aligned}
\frac{dS}{dt}&=-\beta I S\\
\frac{dI}{dt}&=-\beta I S+\gamma I-\omega I\\
\frac{dR}{dt}&=\gamma I\\
N&= S+I+R
\end{aligned}
```

Using Runge–Kutta method to numerically evolve differential equations

https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods

https://www.cs.colorado.edu/~lizb/chaos/ode-notes.pdf

https://iopscience.iop.org/article/10.1088/1742-6596/1040/1/012021/pdf


# Modified SIR Model
---
Takes deaths into account. Essentially divides "R" state into two catagories, recovered and dead.

```math
\begin{aligned}
\frac{dS}{dt}&=-\beta I S\\
\frac{dI}{dt}&=-\beta I S+\gamma I- \omega I\\
\frac{dR}{dt}&=\gamma I\\
\frac{dD}{dt}&=\omega I\\
N&= S+I+R+D
\end{aligned}
```





