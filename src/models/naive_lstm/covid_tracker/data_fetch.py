import requests
import shelve
from pathlib import Path
import datetime
import pandas as pd

if __name__ == "__main__":
    from ct_config import field_list, url, filepath
else:
    from .ct_config import field_list, url, filepath


base_path = str(Path(__file__).parent)

def deshelve():
    db = shelve.open(base_path + "/" + filepath)
    date = db["last_day_updated"]
    state_dict = db["us_data"]
    db.close()
    outdict = {
        "date": date,
        "state_dict": state_dict
    }
    return outdict

def get_states(file):
    state_list = []
    with open(file) as file:
        states = file.readlines()
        for state in states:
            state_list.append(state[:-1])
    return state_list

def get_state_dict(state_list):
    state_dict = {}
    for state in state_list:
        state_dict[state] = {
            "state_name": state,
            "state_data": {},
            "state_data_pd": {}
        }
    return state_dict

def data_processor(data):
    return (0 if data == None else data)

def api_request(state):
    params = {
        "state" : state
    }
    response = requests.get(url,params)
    return response.json()

def data_select(state, fields):
    data_dict = {}
    req_data = api_request(state)
    for field in fields:
        data_dict[field] = []
        for day in req_data:
            day_data = data_processor(day.get(field,0))
            data_dict[field].append(day_data)
        data_dict[field].reverse()
    return data_dict


def fetch_data(field_list=field_list):
    data = deshelve()
    print (data["date"])
    print (datetime.datetime.now().date())
    if data["date"] == datetime.datetime.now().date():
        print ("fetching locally")
        return data["state_dict"]
    else:
        print ("fetching from online")
        state_list = get_states(base_path + "/states.txt")
        state_dict = get_state_dict(state_list)
        for state in state_list:
            state_dict[state]["state_data"] = data_select(state,field_list)
            state_dict[state]["state_data_pd"]= pd.DataFrame.from_dict(state_dict[state]["state_data"])
        return state_dict

def create_pd(data,column):
    pass

def shelve_data(data):
    my_shelve = shelve.open(base_path + "/" + filepath)
    my_shelve["us_data"] = data
    my_shelve["last_day_updated"] = datetime.datetime.now().date()
    my_shelve.close()


if __name__ == "__main__":
    data = fetch_data(field_list)
    print (data['NY'])
    shelve_data(data)
    

