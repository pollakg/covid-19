from covid_tracker.data_fetch import *
from pandas import *
from pandas import DataFrame as df
from pandas import Series
from pandas import concat
from pandas import read_csv
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from math import sqrt
from matplotlib import pyplot as plt
import numpy as np


def XY_creator(data_np,predict_period,train_period):
    data_np = data_np.reshape((len(data_np),1))
    scaler = MinMaxScaler()
    scaler = scaler.fit(data_np)
    for i in range(0,len(data_np)-predict_period-train_period):
        time_series = data_np[i:i+train_period]
        time_series_nom = scaler.transform(time_series)
        time_series_nom = np.reshape(time_series_nom,(1,train_period,feature_num))
        if i == 0:
            X = time_series_nom
        else:
            X = np.append(X,time_series_nom,axis=0)
    
    for i in range(train_period,len(data_np)-predict_period):
        time_series = data_np[i:i+predict_period]
        time_series_nom = scaler.transform(time_series)
        time_series_nom = np.reshape(time_series_nom,(1,predict_period,feature_num))
        if i == train_period:
            Y = time_series_nom
        else:
            Y = np.append(Y,time_series_nom,axis=0)
    return [X,Y]


train_test_split = 0.7
predict_period = 3
train_period =  10
feature_num = 1
batch_num = 27
my_dict = {
    "data":[1,3,6,10,15,21,28,36,45]
}

dummy_pd = pandas.DataFrame(my_dict)

data = fetch_data(field_list)
# print (data["NY"]['state_data_pd'])
pd_data  = data["NY"]['state_data_pd']
beta_list = []
infect_list = data["NY"]['state_data_pd']['positive']
for i in range(1,len(data["NY"]['state_data_pd'])):
    yest_i = data["NY"]['state_data_pd']['positive'][i-1]
    tod_i = data["NY"]['state_data_pd']['positive'][i]
    beta_list.append(tod_i - yest_i)

shelve_data(data)
train_data_len = int(len(beta_list)*train_test_split)
beta_training = beta_list[0:train_data_len]
beta_test = beta_list[-(len(beta_list)-train_data_len):]

# print (beta_training)
# print (beta_test)


beta_training_np = np.array(beta_training)
beta_test_np = np.array(beta_test)

X_train, Y_train = XY_creator(beta_training_np,predict_period,train_period)

X_test, Y_test = XY_creator(beta_test_np,predict_period,train_period)

model = Sequential()
model.add(LSTM(50, activation='relu', return_sequences=True, input_shape=(train_period, feature_num)))
model.add(LSTM(200, activation='relu'))
model.add(Dense(predict_period))

# print (Y_train[:,:,0])

model.compile(optimizer='adam', loss='mse')
training_history = model.fit(
    X_train,
    Y_train[:,:,0],
    epochs=100,
    validation_data=(X_test, Y_test[:,:,0])
)

X_viz_list = beta_training[-10:]
X_viz_np = np.array(X_viz_list)
X_viz_np = X_viz_np.reshape((len(X_viz_np),1))


beta_training_np = beta_training_np.reshape((len(beta_training_np),1))
scaler = MinMaxScaler()
scaler = scaler.fit(beta_training_np)

X_viz_nom = scaler.transform(X_viz_np)
X_viz_nom = np.reshape(X_viz_nom,(1,train_period,feature_num))


trainPredict = model.predict(X_viz_nom)
trainPredict = scaler.inverse_transform(trainPredict)
trainPredict_list = trainPredict[0].tolist()
print (trainPredict_list)

actual_list = beta_list
predict_list = beta_training + trainPredict_list
plt.plot(actual_list,label="actual")
plt.plot(predict_list,label="predict")
plt.legend()
plt.show()


