import matplotlib.pyplot as plt
import seaborn as sns
from .CompartementalModel import BaseClass
import numpy as np
import pandas as pd
from statistics import mean
# import CompartementalModel


class SIR_N_Day_Avg(BaseClass):

    def __init__(self,
                 pred_date=2,
                 backTrackDays=14,
                 country='Switzerland',
                 **args
                 ):

        # Initialize
        # deaths (R)
        deaths_link = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv'
        deathData = pd.read_csv(deaths_link)
        deathData = deathData[deathData['Country/Region'] == country]
        # recovery (R)
        recovery_link = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv'
        recoveryData = pd.read_csv(recovery_link)
        recoveryData = recoveryData[recoveryData['Country/Region'] == country]
        # cases (I)
        cases_link = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'
        casesData = pd.read_csv(cases_link)
        casesData = casesData[casesData['Country/Region'] == country]

        # Initial Conditions
        # Load most recent data
        D0 = deathData.iloc[:, -1].values[0]
        I0 = casesData.iloc[:, -1].values[0]
        R0 = recoveryData.iloc[:, -1].values[0]

        # Aprox. population
        worldData = pd.read_csv("src/data/global_dataset/worldometer.csv")
        N = float(worldData[worldData['Country'] == country][['Population']].values[0][0].replace(',', ''))

        # Determine susceptible pop.
        S0 = N - D0 - I0 - R0
        # Find params over last days
        # Find Data
        deathList = []
        recoveryList = []
        casesList = []
        susceptList = []
        for i in range(backTrackDays+1):
            deathList.append(deathData.iloc[:, -(i+1)].values[0])
            casesList.append(casesData.iloc[:, -(i+1)].values[0])
            recoveryList.append(recoveryData.iloc[:, -(i+1)].values[0])
            susceptList.append(N-deathData.iloc[:, -(i+1)].values[0]
                               - casesData.iloc[:, -(i+1)].values[0]
                               - recoveryData.iloc[:, -(i+1)].values[0]
                               )

        # Find rates of change
        deltaDeathList = []
        deltaRecoveryList = []
        deltaCasesList = []
        deltaSusceptList = []
        for i in range(backTrackDays):
            deltaDeathList.append(deathList[i]-deathList[i+1])
            deltaRecoveryList.append(recoveryList[i]-recoveryList[i+1])
            deltaCasesList.append(casesList[i]-casesList[i+1])
            deltaSusceptList.append(susceptList[i]-susceptList[i+1])

        # Extract Params.
        cList = []
        gammaList = []
        omegaList = []
        for i in range(backTrackDays):
            cList.append(-deltaSusceptList[i]/(susceptList[i-1]*casesList[i-1]/N))
            gammaList.append(deltaRecoveryList[i]/susceptList[i-1])
            omegaList.append(deltaDeathList[i]/susceptList[i-1])

        # Avg. Params.
        c = mean(cList)
        gamma = mean(gammaList)
        omega = mean(omegaList)

        super(BaseClass, self).__init__()
        self.N = N
        self.beta = c
        self.gamma = gamma
        self.omega = omega
        self.I0 = I0
        self.R0 = R0
        self.D0 = D0
        self.S0 = S0
        self.country = country
        self.t = np.linspace(0, pred_date, pred_date)
        self.x0 = (S0, I0, R0, D0)
        self.to_be_saved = {
            "N": N,
            "country": country,
        }

    def dxdt(self, x, t):
        """ governing equations of the SIR ODE
        :param x: input parameter x=[S[t] I[t] R[t]]
        :returns: List of system of ODEs
        """
        S, I, R, D = x
        beta = self.beta
        gamma = self.gamma
        omega = self.omega
        N = self.N

        dSdt = -beta * S * I / N
        dIdt = beta * S * I / N - gamma * I - omega * I
        dRdt = gamma * I
        dDdt = omega * I
        return dSdt, dIdt, dRdt, dDdt

    def run(self):
        """ Calls parent class that runs solve() and save it to self.prd
            Need to specify args to store in to_be_saved, which will be passed
            to save_predictions in the runner.py
        """
        super().run()
        S, I, R, D = self.pred
        self.to_be_saved["R"] = R[-1]
        self.to_be_saved["D"] = D[-1]
        self.to_be_saved["I"] = D[-1]
        self.to_be_saved["S"] = S[-1]

    def plot(self):
        sns.set()
        N = self.N
        S, I, R, D = self.pred

        fig = plt.figure(facecolor='w')
        ax = fig.add_subplot(111, axisbelow=True)
        ax.plot(self.t, S/N, 'y', alpha=0.5, lw=2, label='Susceptible')
        ax.plot(self.t, I/N, 'r', alpha=0.5, lw=2, label='Infected')
        ax.plot(self.t, R/N, 'g', alpha=0.5, lw=2, label='Recovered with immunity')
        ax.plot(self.t, D/N, 'b', alpha=0.5, lw=2, label='Deceased')
        ax.set_xlabel('Time /days')
        ax.set_ylabel('Number (Fraction)')
        ax.set_ylim(0, 1.2)
        ax.yaxis.set_tick_params(length=0)
        ax.xaxis.set_tick_params(length=0)
        ax.grid(b=True, which='major', c='w', lw=2, ls='-')
        legend = ax.legend()
        legend.get_frame().set_alpha(0.5)

        for spine in ('top', 'right', 'bottom', 'left'):
            ax.spines[spine].set_visible(False)
        plt.show()


if __name__ == '__main__':
    sir = SIR_N_Day_Avg(days=30)
    sir.run({
        "days": 30
    })
    sir.plot()
