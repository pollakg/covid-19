import matplotlib.pyplot as plt
import seaborn as sns
from .CompartementalModel import BaseClass
import numpy as np


class SIR(BaseClass):
    # TODO MAYBE MOVE SOME STUFF TO PARENT
    def __init__(self,
                 N=1000,
                 beta=0.2,
                 gamma=1./10,
                 omega=1./100,
                 S0=None, I0=1, R0=0, D0=0,
                 days=200,
                 country="world"
                 ):
        super(BaseClass, self).__init__()
        self.N = N
        self.beta = beta
        self.gamma = gamma
        self.omega = omega
        if(S0 is None):
            S0 = N-I0-R0-D0
        self.t = np.linspace(0, days, days)
        self.x0 = (S0, I0, R0, D0)
        self.country = country
        self.to_be_saved = {
            "N": N,
            "country": country,
        }
        # ========= to_be_saved =============

    def dxdt(self, x, t):
        """ governing equations of the SIR ODE
        :param x: input parameter x=[S[t] I[t] R[t]]
        :returns: List of system of ODEs
        """
        S, I, R, D = x
        beta = self.beta
        gamma = self.gamma
        N = self.N
        omega = self.omega

        dSdt = -beta * S * I / N
        dIdt = beta * S * I / N - gamma * I - omega * I
        dRdt = gamma * I
        dDdt = omega * I
        return dSdt, dIdt, dRdt, dDdt

    def run(self):
        """ Calls parent class that runs solve() and save it to self.prd
            Need to specify args to store in to_be_saved, which will be passed
            to save_predictions in the runner.py
        """
        super().run()
        S, I, R, D = self.pred
        self.to_be_saved["R"] = R[-1]
        self.to_be_saved["D"] = D[-1]

    def plot(self):
        sns.set()
        N = self.N
        S, I, R, D = self.pred

        fig = plt.figure(facecolor='w')
        ax = fig.add_subplot(111, axisbelow=True)
        ax.plot(self.t, S/N, 'b', alpha=0.5, lw=2, label='Susceptible')
        ax.plot(self.t, I/N, 'r', alpha=0.5, lw=2, label='Infected')
        ax.plot(self.t, R/N, 'g', alpha=0.5, lw=2, label='Recovered with immunity')
        ax.plot(self.t, D/N, 'y', alpha=0.5, lw=2, label='Deceased')
        ax.set_xlabel('Time /days')
        ax.set_ylabel('Number (1000s)')
        ax.set_ylim(0, 1.2)
        ax.yaxis.set_tick_params(length=0)
        ax.xaxis.set_tick_params(length=0)
        ax.grid(b=True, which='major', c='w', lw=2, ls='-')
        legend = ax.legend()
        legend.get_frame().set_alpha(0.5)

        for spine in ('top', 'right', 'bottom', 'left'):
            ax.spines[spine].set_visible(False)
        plt.show()


if __name__ == '__main__':
    sir = SIR(days=170)
    sir.run({
        "I0": 10
    })
    sir.plot()
