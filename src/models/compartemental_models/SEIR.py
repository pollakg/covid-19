import matplotlib.pyplot as plt
import seaborn as sns
from .CompartementalModel import BaseClass
import numpy as np
# import CompartementalModel


class SEIR(BaseClass):

    def __init__(self):
        # DO YOUR INITALIZATION STUFF HERE
        pass
        #          N=1000,
        #          beta=0.2,
        #          gamma=1./10,
        #          S0=None, I0=1, R0=0,
        #          days=70
        #          ):
        # # super(CompartementalModel, self).__init__()
        # self.N = N
        # self.beta = beta
        # self.gamma = gamma
        # if(S0 is None):
        #     self.S0 = N-I0-R0
        # else:
        #     self.S0 = S0
        # self.t = np.linspace(0, days, days)
        # self.I0 = I0
        # self.R0 = R0
        # ADD HERE YOUR STUFF

    def dxdt(self, x, t):
        """ governing equations of the SIR ODE
        :param x: input parameter x=[S[t] I[t] R[t]]
        :returns: List of system of ODEs
        """
        pass
        # S, I, R=x
        # beta=self.beta
        # gamma=self.gamma
        # N=self.N

        # dSdt=-beta * S * I / N
        # dIdt=beta * S * I / N - gamma * I
        # dRdt=gamma * I
        # return dSdt, dIdt, dRdt
        # ADD YOUR EQUATIONS

    # def solve(self, ode, t, IC):
    #     # solve ODE
    #     x = odeint(self.dxdt, IC, self.t)
    #     return x

    # def run(self, args=None):
    #     """ runs the model
    #     :param args:
    #     :returns:
    #     :rtype:
    #     """
    #     if isinstance(args, dict):
    #         if "S0" in args.keys():
    #             self["S0"] = args["S0"]
    #         if "I0" in args.keys():
    #             self.I0 = args["I0"]
    #         if "R0" in args.keys():
    #             self.R0 = args["R0"]
    #         if "days" in args.keys():
    #             days = args["days"]
    #             self.t = np.linspace(0, days, days)

    #     pred = self.solve(self.dxdt, self.t, (self.S0, self.I0, self.R0))
    #     S, I, R=pred.T
    #     self.S_pred=S
    #     self.I_pred=I
    #     self.R_pred=R

    def plot(self):
        pass
        # ADD YOUR OWN PLOT THAT YOU LIKE
        # sns.set()
        # S = self.S_pred
        # I = self.I_pred
        # R = self.R_pred
        # N = self.N

        # fig = plt.figure(facecolor='w')
        # ax = fig.add_subplot(111, axisbelow=True)
        # ax.plot(self.t, S/N, 'b', alpha=0.5, lw=2, label='Susceptible')
        # ax.plot(self.t, I/N, 'r', alpha=0.5, lw=2, label='Infected')
        # ax.plot(self.t, R/N, 'g', alpha=0.5, lw=2, label='Recovered with immunity')
        # ax.set_xlabel('Time /days')
        # ax.set_ylabel('Number (1000s)')
        # ax.set_ylim(0, 1.2)
        # ax.yaxis.set_tick_params(length=0)
        # ax.xaxis.set_tick_params(length=0)
        # ax.grid(b=True, which='major', c='w', lw=2, ls='-')
        # legend = ax.legend()
        # legend.get_frame().set_alpha(0.5)

        # for spine in ('top', 'right', 'bottom', 'left'):
        #     ax.spines[spine].set_visible(False)
        # plt.show()


if __name__ == '__main__':
    seir = SEIR(days=170)
    seir.run({
        "I0": 10
    })
    seir.plot()
