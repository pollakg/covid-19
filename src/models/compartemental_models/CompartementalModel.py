from scipy.integrate import odeint
import abc


class BaseClass():
    __metaclass__ = abc.ABCMeta

    def __init__(self, args):
        pass

    @abc.abstractmethod
    def dxdt(x, t):
        """ governing ODE of the Model
        :param x: input parameter x=[S[t] I[t] R[t]]
        :returns: List of system of ODEs
        :param x: input list or vector of parameters
        :param t: time variable 
        :returns: tuple, list, vectors of differentials 
        """

    def solve(self):
        """ Solves ODE
        :param self.t: Time range for which to solve 
        :param self.x0: Tuple or list of inital conditions,
                   should be set in constructor of derived class
        :returns: solution of ODE

        """
        return odeint(self.dxdt, self.x0, self.t)

    def run(self):
        """ runs the model
            and save preictions in predict
        """
        self.pred = self.solve().T
