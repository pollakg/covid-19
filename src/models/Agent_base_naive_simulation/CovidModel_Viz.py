from CovidModel import *
from mesa.visualization.modules import CanvasGrid
from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.modules import ChartModule

num_agent = 200
canvas_width = 100
canvas_height = 100
infect_prob = 0.7
radius = 5
recovery = 14

def agent_portrayal(agent):
    portrayal = {"Shape": "circle",
                 "Filled": "true",
                 "r": 0.5}

    if agent.infect_state == 0:
        portrayal["Color"] = "green"
        portrayal["Layer"] = 0
    elif agent.infect_state == 1:
        portrayal["Color"] = "red"
        portrayal["Layer"] = 0
    elif agent.infect_state == 2:
        portrayal["Color"] = "grey"
        portrayal["Layer"] = 0
    return portrayal

grid = CanvasGrid(agent_portrayal, canvas_width, canvas_height, 500, 500)
chart = ChartModule(
    [
        {
            "Label": "S",
            "Color": "Green",
        },
        {
            "Label": "I",
            "Color": "Red"
        },
        {
            "Label": "R",
            "Color": "Grey"
        },

    ],
    data_collector_name='datacollector'
)

server = ModularServer(CovidModel,
                       [grid, chart],
                       "Covid Model",
                       {
                           "N":num_agent,
                           "width":canvas_width,
                           "height":canvas_height,
                           "p":infect_prob,
                           "radius":radius,
                           "recovery":recovery
                       })


server.port = 8521 # The default
server.launch()