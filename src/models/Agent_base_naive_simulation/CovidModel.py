from mesa import Agent, Model
from mesa.space import MultiGrid
from mesa.time import SimultaneousActivation
from mesa.datacollection import DataCollector
import numpy as np


# Infected states:
# 0: susceptable
# 1: Infected
# 2: recovered/removed
  
def compute_I(model):
    agent_infect_state = [agent.infect_state for agent in model.schedule.agents]
    I_num = 0
    for i in agent_infect_state:
        if i == 1:
            I_num += 1
    return I_num

def compute_S(model):
    agent_infect_state = [agent.infect_state for agent in model.schedule.agents]
    S_num = 0
    for i in agent_infect_state:
        if i == 0:
            S_num += 1
    return S_num

def compute_R(model):
    agent_infect_state = [agent.infect_state for agent in model.schedule.agents]
    R_num = 0
    for i in agent_infect_state:
        if i == 2:
            R_num += 1
    return R_num

class CovidAgent(Agent):
    def __init__(self,unique_id,model,p,radius,recovery):
        super().__init__(unique_id,model)
        self.infect_state = 0
        self.infected_days = 0
        self.infect_prob = p
        self.radius = radius
        self.recovery_day = recovery
    
    def move(self):
        possible_steps = self.model.grid.get_neighborhood(
            self.pos,
            moore=True,
            include_center = True
        )
        new_position = self.random.choice(possible_steps)
        self.model.grid.move_agent(self,new_position)

    def infect(self):
        ppl_nearby = self.model.grid.get_neighborhood(
            self.pos,
            radius = self.radius,
            moore=True,
            include_center = True
        )
        cellmates = self.model.grid.get_cell_list_contents(ppl_nearby)
        for cellmate in cellmates:
            num = np.random.uniform(0,1)
            if num < self.infect_prob and cellmate.infect_state == 0:
                cellmate.infect_state = 1

    def recovery(self):
        if self.infected_days < self.recovery_day:
            self.infected_days += 1
        else:
            self.infected_days = 0
            self.infect_state = 2

    def step(self):
        self.move()
        if self.infect_state == 1:
            self.infect()
            self.recovery()

    def advance(self):
        pass


class CovidModel(Model):
    def __init__(self, N, width, height, p, radius, recovery):
        self.num_agents = N
        self.grid = MultiGrid(width, height, True)
        self.schedule = SimultaneousActivation(self)
        self.running = True

        for i in range(self.num_agents):
            a = CovidAgent(i, self,p,radius,recovery)
            if i < 10:
                a.infect_state = 1
            self.schedule.add(a)
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            self.grid.place_agent(a, (x, y))

        self.datacollector = DataCollector(
            model_reporters={
                "I": compute_I,
                "S": compute_S,
                "R": compute_R
            }
        )

        
    def step(self):
        self.datacollector.collect(self)
        self.schedule.step()
    



    

    