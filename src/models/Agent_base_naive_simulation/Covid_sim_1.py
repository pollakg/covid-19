import CovidModel as cm
from mesa.datacollection import DataCollector
import matplotlib.pyplot as plt




num_agent = 200
canvas_width = 100
canvas_height = 100
infect_prob = 0.7
radius = 5
recovery = 14


model = cm.CovidModel(num_agent,canvas_width,canvas_height,infect_prob,radius,recovery)
for i in range(100):
    if i % 10 == 0:
        print ("Iter: :"+ str(i))
    model.step()


data = model.datacollector.get_model_vars_dataframe()

print(type(data))
print(data)

data.plot()
plt.show()

