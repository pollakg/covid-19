import CovidModel_2 as cm
from mesa.datacollection import DataCollector
import matplotlib.pyplot as plt
from mesa.batchrunner import BatchRunner
import pandas as pd


num_agent = 200
canvas_width = 100
canvas_height = 100
infect_prob = 0.7
radius = 4
recovery = 14

time_steps = 40
iterations = 3

fixed_params = {
    "N": num_agent,
    "width": canvas_width,
    "height":canvas_height,
    "p": infect_prob
}

variable_params = {
    "recovery": [7,14],
    "radius": [3,4]
}



def data_averager(dataframe_rows):
    S = pd.DataFrame()
    I = pd.DataFrame()
    R = pd.DataFrame()
    run_num = 0
    for i in dataframe_rows["Data Collector"]:
        S["run:"+str(run_num)]=i.get_model_vars_dataframe()["S"]
        I["run:"+str(run_num)]=i.get_model_vars_dataframe()["I"]
        R["run:"+str(run_num)]=i.get_model_vars_dataframe()["R"]
        run_num += 1
    combine_df = pd.DataFrame()
    combine_df["S"] = S.mean(axis=1)
    combine_df["I"] = I.mean(axis=1)
    combine_df["R"] = R.mean(axis=1)
    return combine_df


def batch_data_processor(run_data,iterations,variable_parameters):
    out_dataframes = []
    for i in range( int(len(run_data)/iterations) ):
        start = int(i*iterations)
        end = int(i*iterations + iterations)
        sir_data = data_averager(run_data.iloc[start:end])
        meta_data = {}
        for key in variable_parameters.keys():
            meta_data[key] = run_data[key][start]
        out_dataframes.append({
            "data": sir_data,
            "meta_data": meta_data
        })
    return out_dataframes

batch_run = BatchRunner(
    cm.CovidModel,
    variable_parameters=variable_params,
    fixed_parameters=fixed_params,
    iterations= iterations,
    max_steps=time_steps,
    model_reporters={
        "Data Collector": lambda m: m.datacollector
    }
)

def run_sim():
    batch_run.run_all()
    run_data = batch_run.get_model_vars_dataframe()
    # run_data.head()
    processed_data = batch_data_processor(run_data,iterations,variable_params)
    return processed_data

def plot_sim(processed_data):
    # fig = plt.figure()
    for frame in processed_data:
        plt.plot(frame["data"])
    plt.show()


if __name__ == "__main__":
    data = run_sim()
    plot_sim(data)

