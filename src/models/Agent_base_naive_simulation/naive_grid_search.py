from covid_tracker import data_fetch as ct_df
import matplotlib.pyplot as plt
import Covid_sim_2 as covid_sim

field_list = ["date","positive","hospitalized","recovered","death"]


data_dict = ct_df.fetch_data(field_list)
ct_df.shelve_data(data_dict)
NY_df = data_dict["NY"]["state_data_pd"]
NY_pop = 8400000

NY_df["R"] = NY_df["recovered"]+ NY_df["death"] + NY_df["hospitalized"]
NY_df["I"] = NY_df["positive"] - NY_df["hospitalized"]
NY_df["S"] = NY_pop - NY_df["I"] - NY_df["R"] - NY_df["hospitalized"]

# print (NY_df)

# plot_fields = ["I","R"] 

# for field in plot_fields:
#         plt.plot(NY_df[field])
# plt.legend(plot_fields)
# plt.show()



