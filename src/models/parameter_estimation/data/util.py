import requests
import shelve
import datetime
import pandas as pd
import csv
from pycountry import countries
def remove_duplicates(list):
    new={}
    for d in list:
        new.update({d['policy_type_code']: dict(d.items())})
    return new
def change_to_alpha_3(list):
    index=[]
    for country in list:
        try:
            index.append(countries.lookup(country).alpha_3)
        except LookupError:
            index.append(country)
    return index
def get_list_of_countries(path,*argv):
    f=open(path, 'rb')
    ls=f.read().decode("utf-8").split(",")
    if len(argv) != 0 :
        intersect=set(ls)&set(change_to_alpha_3(argv[0]['infected'].index))&set(change_to_alpha_3(argv[1].index))
        return list(intersect)
    else:
        return ls
def make_columns(column_fields,include):
    measure_columns=[i+"_"+j for i in column_fields["policyActions"] for j in ["policyvalue","isgeneral"]]
    if include:
        return column_fields["stringencyData"]+measure_columns
    else:
        return column_fields["stringencyData"]
def put_in_dict(ret):
    dict={}
    for out in ret:
        try:
            dict[out["stringencyData"]["country_code"]]= out
        except KeyError as K:
            continue
    return dict

def get_datevector(last,today):
    return pd.date_range(last, today).strftime("%Y-%m-%d").tolist()
def get_country_dict(country_list,today,column_fields,startdate,include_policy=False):
    country_dict = {}
    index=get_datevector(startdate,today)
    columns=make_columns(column_fields,include_policy)
    for country in country_list:
        country_dict[country] ={"TimeSeries":pd.DataFrame(index=index),
                                "MetaData":None}
    return country_dict