column_fields={"stringencyData":["stringency"],
               "policyActions":   ["S1","S2","S3","S4","S5","S6","S7","S12","S13"]
               }

url_ts = "https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/date-range/2020-01-01"
url_country = "https://covidtrackerapi.bsg.ox.ac.uk/api/vstringency/actions/"
db_name = "oxford_db"
link_apple="https://raw.githubusercontent.com/ActiveConclusion/COVID19_mobility/master/apple_reports/applemobilitytrends.csv"
link_wm="/worldometer.csv"
link_coverage='https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/testing/covid-testing-all-observations.csv'
country_path="/countries.csv"
link_hopkins={
    "recovered":"https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv",
    "death":'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv',
    "infected":'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'
}
backup_oxford='/OxCGRT_timeseries_all.xlsx'