from pathlib import Path
import datetime
from pycountry import countries
import pandas as pd
import numpy as np
import requests
import eventlet
if __name__ == "__main__":
    from util import get_list_of_countries, get_country_dict
    from config import column_fields,  link_wm, link_hopkins, link_apple, country_path, \
        link_coverage, url_ts,backup_oxford
else:
    from src.models.parameter_estimation.data.util import  get_list_of_countries, get_country_dict
    from .config import column_fields, link_wm, link_hopkins, link_apple, country_path, \
        link_coverage, url_ts,backup_oxford

today = datetime.date.today()
base_path = str(Path(__file__).parent)
fields = ['Population', 'Density', 'Land Area', 'Med. Age', 'Urban Population (percent)']
def p2f(x):
    try:
        return float(x.strip('%'))/100
    except:
        return 0.5
def get_country_meta(country_dict):
    """
    Get metadata about country
    :param country:
    :return: pandas dataframe with rows: 'Country or region', 'Tests', 'Positive', 'Tests /millionpeople',
       'Positive /thousandtests', 'Population', 'Density', 'Land Area',
       'Med. Age', 'Urban Population (percent)'
    """
    data = pd.read_csv(base_path + link_wm, index_col=0,thousands=',',converters={'Urban Population (percent)':p2f})

    for country in country_dict:
        df=country_dict[country]
        try:

            cn = countries.get(alpha_3=country).name
            df["MetaData"] = data.loc[cn, fields]
            df["MetaData"]["exceeded20"]=df['TimeSeries'][df['TimeSeries']['infected'].gt(20)].index[0]
        except (KeyError, AttributeError) as e:
            continue


def add_column(country_dict, df_column, column_name, has_alpha3=False, complete=False):
    for country in list(country_dict):
        try:
            if not has_alpha3:
                cn = countries.get(alpha_3=country).name
            else:
                cn =country
            country_dict[country]['TimeSeries'][column_name] = df_column.loc[cn, :]
        except (KeyError, AttributeError) as e:
            if complete:
                del country_dict[country]
            elif column_name == 'coverage':
                country_dict[country]['TimeSeries'][column_name] = 0
            else:
                continue


def calc_beta(country_dict):

    for country in country_dict:
        c = country_dict[country]['TimeSeries']
        infected = c['infected']
        recovered = c['recovered']
        deaths=c['death']
        c['gamma'] = ((recovered.shift(-1) - recovered) / infected).fillna(method='ffill').replace(0.0,method='ffill')
        c['beta'] = ((infected.shift(-1) - infected ) / infected).fillna(
            method='ffill').replace(0.0,method='ffill')
        c['delta'] = ((deaths.shift(-1) - deaths)/infected).fillna(method='ffill').replace(0.0,method='ffill')
        c['beta'].fillna(0, inplace=True)
        c['gamma'].fillna(0, inplace=True)
        c['beta'].replace(np.inf, 1.0, inplace=True)
        c['gamma'].replace(np.inf, 1.0, inplace=True)
        c['delta'].fillna(0, inplace=True)
        c['delta'].replace(np.inf, 1.0, inplace=True)




def get_hopkins_df():
    df_hopkins = {}

    avg={'recovered':15,'death':6,'infected':6}
    for link in link_hopkins:
        df_hopkins[link] = pd.read_csv(link_hopkins[link], parse_dates=True).groupby(['Country/Region'])
        df_hopkins[link] = df_hopkins[link].sum().drop(['Lat', 'Long'], axis=1)
        df_hopkins[link].columns = pd.to_datetime(df_hopkins[link].columns).strftime('%Y-%m-%d')
        df_hopkins[link]=df_hopkins[link].T.replace(to_replace=0, method='ffill').T #TODO: make this thing work. currently it produces inf
        df_hopkins[link]=df_hopkins[link].rolling(avg[link],win_type='boxcar',axis=1).mean()
    return df_hopkins

def get_stringency_df(end=today):
    url = url_ts + "/"+end.strftime("%Y-%m-%d")
    try:
        response = requests.get(url,timeout=25).json()

        dict={date: {cn:response["data"][date][cn]["stringency"] for cn in response["data"][date]} for date in response["data"]}
        df_stringency = pd.DataFrame.from_dict(dict)
        df_stringency.fillna(value=np.nan, inplace=True)

    except (KeyError,requests.exceptions.ReadTimeout) as e:
        print('No download from oxford API possible. Will use already existing data. Please try first to download the most recent version of \'OxCGRT_timeseries_all.xls\'')
        df_stringency=pd.read_excel(base_path+backup_oxford,parse_dates=True)
        df_stringency.index=df_stringency['CountryCode']
        df_stringency.drop(columns=['CountryName','CountryCode'],inplace=True)
        df_stringency.columns=pd.to_datetime(df_stringency.columns).date
        df_stringency=df_stringency.head(-3)
    df_stringency=df_stringency/100
    df_stringency.fillna(method='ffill', inplace=True, axis=1)
    df_stringency.fillna(method='bfill', inplace=True, axis=1)
    return df_stringency

def get_mobility_df():
    df_mobility = pd.read_csv(link_apple, parse_dates=True).groupby(['region']).mean()
    df_mobility.columns = pd.to_datetime(df_mobility.columns).strftime('%Y-%m-%d')
    df_mobility=(df_mobility-df_mobility.min())/(df_mobility.max()-df_mobility.min())
    return df_mobility


def get_coverage_df():
    cols = ['Entity', 'Date', 'Cumulative total per thousand']
    df_coverage = pd.read_csv(link_coverage, parse_dates=True, usecols=cols)
    df_coverage = df_coverage.pivot(index='Date', columns='Entity', values='Cumulative total per thousand')
    df_coverage.columns = [col.split(' - ')[0] for col in df_coverage.columns]
    df_coverage.fillna(method='ffill', inplace=True)
    df_coverage.fillna(0, inplace=True)
    df_coverage = df_coverage.loc[:, ~df_coverage.columns.duplicated()]
    return df_coverage.T

def get_weather_df():
    cols = ["Key", "Date", "MinimumTemperature", "MaximumTemperature"]
    data = pd.read_csv("https://open-covid-19.github.io/data/weather.csv", usecols=cols)
    data['temp_avg'] = data[["MinimumTemperature", "MaximumTemperature"]].mean(axis=1)
    data = data[np.invert(data.Key.str.contains("_|-").fillna(True).values)]
    data = data.pivot(index='Key', columns='Date', values='temp_avg')
    cn_list = list(data.index.values)
    for i, cn in enumerate(cn_list):
        try:
            cn_list[i] = countries.get(alpha_2=cn).alpha_3
        except AttributeError:
            cn_list.remove(cn)
            data.drop(cn, inplace=True)
    data.index = cn_list
    data.fillna(method='ffill', inplace=True, axis=1)
    data.fillna(method='bfill', inplace=True, axis=1)
    data = (data - data.min()) / (data.max() - data.min())
    return data



def fill_na(country_dict):
    for country in country_dict:
        country_dict[country]['TimeSeries'].fillna(method='ffill', axis=0, inplace=True)


def remove_incomplete_country(country_dict,input_features = ['stringency','mobility','temperature'] ):
    # print (type(country_dict['ESP']['TimeSeries']))
    bad_countries = []
    for country in country_dict:
        df = country_dict[country]['TimeSeries']
        if not set(input_features).issubset(df.columns):
            bad_countries.append(country)
    print (bad_countries)
    for bad_country in bad_countries:
        del country_dict[bad_country]


def get_df(countries=None, end=today,**kwargs):
    """
    Returns a dict of countries with metdta and timeseries
    :param json: Data which you get from get_data()
    :param countries: List of countries in alpha_3 coding (e.g. Switzerland=CHE)
            A list is not needed but is highly recommended to save memory:)
    :return: dictionary of countries where each country has stored a Pandas DataFrame in "TimeSeries"
                and country metadata in "MetaData"
    """
    if 'include_policies' in kwargs:
        incl = kwargs.get('include_policies')
    else:
        incl = False
    if 'complete_data' in kwargs:
        complete_data = kwargs.get('complete_data')
    else:
        complete_data = False
    if 'startdate' in kwargs:
        startdate = datetime.datetime.strptime(kwargs.get('startdate'), "%Y-%m-%d")
    else:
        # Probably first date of available data?
        startdate = datetime.datetime.strptime('2020-01-22', "%Y-%m-%d")
    df_mobility = get_mobility_df()
    df_hopkins = get_hopkins_df()
    df_coverage = get_coverage_df()
    df_stringency=get_stringency_df()
    df_weather=get_weather_df()

    if countries:
        list_of_countries = countries
    else:
        list_of_countries = get_list_of_countries(base_path + country_path, df_hopkins, df_mobility)
    country_dict = get_country_dict(list_of_countries, end, column_fields, startdate, include_policy=incl)

    add_column(country_dict, df_stringency,"stringency",has_alpha3=True)
    add_column(country_dict, df_mobility, "mobility", complete=complete_data)
    add_column(country_dict, df_coverage, "coverage")
    add_column(country_dict,df_weather,"temperature",has_alpha3=True)
    for df in df_hopkins:
        add_column(country_dict, df_hopkins[df], df, complete=complete_data)
    get_country_meta(country_dict)
    remove_incomplete_country(country_dict)
    fill_na(country_dict)
    calc_beta(country_dict)
    return country_dict


def build_training_data(country_dict, history_size = 30, target_size = 7,time_machine=7):
    labels = []
    data=[]
    meta= []
    # for country in country_dict:
    #     print(country)
    for cn in country_dict.values():
        offset = cn['TimeSeries'].index.get_loc(cn['MetaData']["exceeded20"])  # only look at data when 20 cases are exceeded
        start_index = offset+history_size
        # end_index = len(country_dict['CHE']['TimeSeries'] - 1) - target_size
        end_index = len(country_dict['ARG']['TimeSeries'] - 1) - target_size -time_machine
        for i in range(start_index, end_index):
            val=cn['TimeSeries'][['beta','stringency','mobility','temperature','delta']].to_numpy()[i - history_size : i]
            data.append(val)
            # labels.append(cn['TimeSeries'][['beta','coverage','gamma','delta']].to_numpy()[i:i + target_size])
            labels.append(cn['TimeSeries'][['beta','delta']].to_numpy()[i:i + target_size])
            meta.append(cn["MetaData"][fields].to_numpy(dtype=np.float32))
    data = np.array(data)
    labels = np.array(labels)

    meta=np.array(meta)

    return data, labels, meta

def get_predict_data(country_dict,history_size=20,time_machine=7):
    data = []
    meta = []
    columns=['beta', 'stringency', 'mobility', 'temperature','delta','infected','death']
    countries=[]
    for cn in country_dict:
        date=np.array(country_dict[cn]['TimeSeries'].index)[-time_machine-history_size-1:-1-time_machine].reshape(history_size,1)
        b=country_dict[cn]['TimeSeries'][columns].to_numpy()[-time_machine-history_size-1:-1-time_machine]
        data.append(np.hstack((b,date)))
        meta.append(country_dict[cn]["MetaData"][fields].to_numpy(dtype=np.float32))
        countries.append(cn)
    data=np.array(data)
    meta = np.array(meta)


    return data, meta,countries
    
def get_np(history_size=20,target_size=10,end=today,time_machine=7):
    """
    :param history_size: steps into past for training
    :param target_size: days into future for prediction 
    :param end: 
    :returns: X, y, meta, X_pred, meta_pred, countries
    :rtype: Tuple
    """
    df = get_df(include_policies=False, complete_data=True)
    X, y, meta =build_training_data(df, history_size=history_size, target_size=target_size,time_machine=time_machine)
    X_pred,meta_pred, countries=get_predict_data(df,history_size=history_size,time_machine=time_machine)
    return  X,y, meta,X_pred,meta_pred, countries


if __name__ == "__main__":
    X,y, meta,X_pred,meta_pred, countries = get_np()
    # print("done")
    # get_df()
    # get_weather_df()