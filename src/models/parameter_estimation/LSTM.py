from datetime import datetime,timedelta
from pycountry import countries as cn_dict
import numpy as np
import os
import pandas as pd
from datetime import datetime,timedelta, date
from pathlib import Path
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
# Recurrent Neural Network
import keras

from keras.models import  Model
from keras.layers import Input,Dense,concatenate,LSTM,Flatten


from sklearn.preprocessing import MinMaxScaler


import sys

sys.path.append(sys.path[0]+'/../../')

from .data.data_fetch import get_np

# scales the data inplace and returns list of scalers
def scale(X_train, X_test, y_train, y_test,meta_train,meta_test):
    X_feat_num = X_train.shape[2]
    y_feat_num = y_train.shape[2]
    X_scaler = [MinMaxScaler(copy=False) for i in range(X_feat_num)]
    Y_scaler = [MinMaxScaler(copy=False) for i in range(y_feat_num)]
    meta_scaler = MinMaxScaler(copy=False)

    for i in range(X_feat_num):
        X_scaler[i].fit(X_train[:, :, i])
        X_scaler[i].transform(X_train[:,:,i])
        X_scaler[i].transform(X_test[:,:,i])

    for i in range(y_feat_num):
        Y_scaler[i].fit(y_train[:,:,i])
        Y_scaler[i].transform(y_train[:,:,i])
        Y_scaler[i].transform(y_test[:,:,i])

    meta_scaler.fit(meta_train)
    meta_scaler.transform(meta_train)
    meta_scaler.transform(meta_test)

    return X_scaler , Y_scaler, meta_scaler
#calculates infected and recovered based on beta and gamma estimates
def infected_death(betas,deltas,infected,death):
    pred_inf=[infected]
    pred_dea=[death]
    for i in range(len(betas[0])):
        pred_inf.append((1 + betas[0][i]) * pred_inf[-1])
        pred_dea.append((1+deltas[0][i])*pred_dea[-1])

    pred_inf=np.array(pred_inf[1:])
    pred_dea=np.array(pred_dea[1:])
    return pred_inf,pred_dea

def get_datevector(first,last):
    return pd.date_range(first, last).strftime("%Y-%m-%d").tolist()












class LSTMModel():
    def __init__(self, time_steps_predict, epoch_num=100,SAVE_PREDICTION=True,split=0.8,time_steps_train=20,monitor=True,time_machine=7,loss_func='mse'):
        self.time_steps_predict = time_steps_predict
        self.split = split
        self.time_steps_train = time_steps_train
        self.epoch_num = epoch_num
        self.countries=None
        self.model=None
        self.samples =0
        self.timesteps=0
        self.feature_dim=0
        self.SAVE_PREDICTION=SAVE_PREDICTION
        self.monitor=monitor
        self.time_machine=time_machine
        self.loss_func = loss_func
    def change_countries(self):
        fullnames=[]
        for cn in self.countries:
            fullnames.append(cn_dict.lookup(cn).name)
        self.countries=fullnames
    def preprocess(self):
        X, y, meta, X_pred, meta_pred, self.countries = get_np(history_size=self.time_steps_train, target_size=self.time_steps_predict,time_machine=self.time_machine)

        self.samples, self.timesteps, self.feature_dim = X.shape

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=1 - self.split, random_state=42)
        meta_train, meta_test = train_test_split(meta, test_size=1 - self.split, random_state=42)

        X_scaler, Y_scaler, meta_scaler = scale(X_train, X_test, y_train, y_test, meta_train, meta_test)

        assert not np.any(np.where(X_train > 1.1) or np.where(X_train < 0.0))
        assert not np.any(np.where(y_train > 1.1) or np.where(y_train < 0.0))
        assert not np.any(np.where(meta_train > 1.1) or np.where(meta_train < 0.0))
        assert not np.any(np.isnan(X))
        assert not np.any(np.isnan(y))
        assert not np.any(np.isnan(meta))
        print('X_train shape:', X_train.shape)
        print('X_test shape: ', X_test.shape)

        return X_train, X_test, y_train, y_test, meta_train, meta_test, X_scaler, Y_scaler, meta_scaler, X_pred, meta_pred
    def __monitor_training(self,history):
        print("History_dict: ", history)

        # summarize history for loss
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('model loss {}/{}'.format(self.time_steps_train,self.time_steps_predict))
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        # plt.show()

    def train(self, X_train, X_test, y_train, y_test, meta_train, meta_test):
        input_meta = Input(shape=(meta_train.shape[1],), name='input_meta')
        input_ts = Input(shape=(self.time_steps_train, self.feature_dim,), name='input_ts')
        input_tests = Input(shape=(self.time_steps_train, 1,), name='input_tests')  # currently not used
        # the first branch operates on the timeseries
        x = LSTM(200, activation='relu', return_sequences=True)(input_ts)
        x = LSTM(200, activation='relu')(x)

        dynamic_out = (x)
        # combine the output of the two branches
        combined = concatenate([dynamic_out, input_meta])

        output_beta = Dense(self.time_steps_predict, activation="relu", name='output_beta')(combined)
        output_delta = Dense(self.time_steps_predict, activation="relu", name='output_delta')(combined)

        model = Model(inputs=[input_meta, input_ts], outputs=[output_beta, output_delta])
        
        model.compile(optimizer='adam', loss=self.loss_func)
        # model.compile(optimizer='adam', loss='mse')
        # model.compile(optimizer='adam', loss='binary_crossentropy')
        training_history = model.fit(
            {'input_meta': meta_train, 'input_ts': X_train},  # ,'input_tests':y_train[:,:,1]},
            {
                'output_beta': y_train[:, :, 0],
                'output_delta': y_train[:, :, 1]
            },
            epochs=self.epoch_num,
            validation_data=({'input_meta': meta_test, 'input_ts': X_test},
                             {
                                 'output_beta': y_test[:, :, 0],
                                 'output_delta': y_test[:, :, 1]
                             })
            # callbacks=[tensorboard_callback],
        )


        if self.monitor:
            self.__monitor_training(training_history)
        self.model=model

    def predict(self,X_scaler, Y_scaler, meta_scaler, X_pred,meta_pred):
        for i in range(len(X_scaler)):
            X_scaler[i].transform(X_pred[:, :, i])

        meta_scaler.transform(meta_pred)

        predicted_beta = []
        predicted_delta = []

        for index, country in enumerate(X_pred[:, :, 0:5]):
            country_ts = np.reshape(country, (1, self.time_steps_train, self.feature_dim))
            country_meta = np.reshape(meta_pred[index, :], (1, 5))
            pred_output = self.model.predict({'input_meta': country_meta, 'input_ts': country_ts})
            predicted_beta.append(Y_scaler[0].inverse_transform(pred_output[0]))
            predicted_delta.append(Y_scaler[1].inverse_transform(pred_output[1]))

        predicted_infected = []
        predicted_death = []
        for i, country in enumerate(X_pred):
            last_data_death = country[-1, 6]
            last_data_infected = country[-1, 5]
            inf, dea = infected_death(predicted_beta[i], predicted_delta[i], last_data_infected, last_data_death)
            predicted_infected.append(inf)
            predicted_death.append(dea)

        predicted_beta = np.array(predicted_beta)

        first_day = datetime.strptime(X_pred[0, 0, -1], "%Y-%m-%d")
        last_day = first_day + timedelta(days=self.time_steps_train + self.time_steps_predict-1)
        datevector = get_datevector(first_day, last_day)

        self.change_countries()
        result_infected = pd.DataFrame(data=predicted_infected, index=self.countries)
        result_death = pd.DataFrame(data=predicted_death, index=self.countries)

        previous_infected = pd.DataFrame(data=X_pred[:, :, 5], index=self.countries)
        previous_death = pd.DataFrame(data=X_pred[:, :, 6], index=self.countries)
        comb_infected = pd.concat((previous_infected, result_infected), axis=1, ignore_index=True)
        comb_death = pd.concat((previous_death, result_death), axis=1, ignore_index=True)
        comb_infected.columns = datevector
        comb_death.columns = datevector
        return result_infected, result_death,last_day

    def plot(self,comb_infected,comb_death):

        for index, row in comb_infected.iterrows():
            plt.figure()
            plt.title(index)
            comb_infected.loc[index, :].plot()
            comb_death.loc[index, :].plot()
            plt.savefig('plots/{}.png'.format(index))
            plt.close()

    def __save_pred(self,df, today, pred_date, filename="prediction", save_dir="results"):
        VARIABELS_TO_BE_ESTIMATED = ["Province/State",
                                     "Country",
                                     "Target/Date",
                                     "N",
                                     "low95N",
                                     "high95N",
                                     "R",
                                     "low95R",
                                     "high95R",
                                     "D",
                                     "low95D",
                                     "high95D",
                                     "T",
                                     "low95T",
                                     "high95T",
                                     "M",
                                     "low95M",
                                     "high95M",
                                     "C",
                                     "low95C",
                                     "high95C",
                                     ]
        days = (pred_date - today).days
        filename = str(days) + "day"+"_"+filename + "_" + pred_date.strftime("%Y-%m-%d") +".csv"
        fp = Path(save_dir) / filename
        fh = open(fp, "w")
        # Write the header line
        for var in VARIABELS_TO_BE_ESTIMATED:
            fh.write(var)
            fh.write(",")
        fh.write("\n")
        df[['N','D']]=df[['N','D']].astype(int)
        df['Target/Date']=pred_date
        keys = df.columns
        for _, row in df.iterrows():
            for var in VARIABELS_TO_BE_ESTIMATED:
                if var in keys:
                    fh.write(str(row[var]) + ",")
                else:
                    fh.write(",")
            fh.write("\n")
        fh.close()

    def save(self,result_infected, result_death,pred_date):
        if self.SAVE_PREDICTION:
            df = pd.concat([result_infected.iloc[:, -1], result_death.iloc[:, -1]],
                           axis=1,
                           keys=['N', 'D'])
            df["Country"] = result_infected.index

            today = pred_date -timedelta(self.time_steps_predict)
            pred_date = pred_date
            self.__save_pred(df, today, pred_date)
    def run(self):
        X_train, X_test, y_train, y_test, meta_train, meta_test, X_scaler, Y_scaler, meta_scaler, X_pred, meta_pred=self.preprocess()
        self.train( X_train, X_test, y_train, y_test, meta_train, meta_test)
        result_infected, result_death, prediction_day=self.predict(X_scaler, Y_scaler, meta_scaler, X_pred,meta_pred)
        self.save(result_infected, result_death, prediction_day)

if __name__ == '__main__':
    lstm=LSTMModel(2,True)
    lstm.run()