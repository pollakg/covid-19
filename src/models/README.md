# Compartmental models 

## The SIR model  

<div align="left">  
<img src="../../figures/sir.png" width=500px>
</div>

#### Equations 
The SIR model is a system of three coupled non-linear ODEs, first dirived by [kermack and mckendrick][^2].

```math
\begin{aligned}
\frac{dS}{dt}&=-\beta \frac{I S}{N}\\
\frac{dI}{dt}&=-\beta \frac{I S}{N}+\gamma I-[\mu I]\\
\frac{dR}{dt}&=\gamma I\\
N&\approx S+I+R&\beta,\gamma>0
\end{aligned}
```
From $`N=`$const it follows that is sufficient to solve only a system of two ODEs since:
 ```math
R(t)=N=S(t)-I(t)
```
**Notes**: 
- For certain cases the system of ODEs can be solve analytically [3],[4].
- $`\mu`$ is the death rate which is proportional to the number of births and death but can roughly be assumed to be constant for a temporary disease. 
#### Parameters
$`\beta`$:	controls how often a susceptible-infected contact results in a new infection i.e. how contagious the disease is.  
$`\gamma`$: controls the rate an infected recovers i.e. how much time it takes untill an infected  recovers.  
$`\gamma`$: is invers proportional to *the number of days it takes to recover*
```math
D=\frac{1}{\gamma}
```
##### The Basic Reproduction Number $`R_0`$
Is the average number of people that a single infected individual infects, when put into a fully suceptible population.  
*Thus* $`R_0`$ tells us about the initial rate of spread of the disease:
```math
R_0=\frac{\beta}{\gamma}=\gamma D\left\{
\begin{aligned}
    &>0 \text{ there will be an epidemic} \\ &=1 \text{ steady state}\\ &<0 \text{ the infections will die out}
\end{aligned}\right.
```
### Herd immunity
Is a form of a indirect immunity to a disease by the fact that the proportion of 
immune individuals is so much bigger then the proportion of infectious, as well as suceptible individual s.t. is very unlickly to get infected. 
<div align="center">  
<img src="../../figures/herd_immunity.png" width="400px">
</div>
  

#### Herd Immunity Ratio/Threshold (HIT) $`p_c`$
Is the neccesary threshold of immune people $`$p_c`$ (i.e. vaccinated or recovered)
s.t. we obtain a steady state.
Let $`p`$ be the number of immune people in gerneral.
If the basic reproduction number multiplied times the number of suceptible people $`1-p`$ 
equals one we will have a stready state from which follows:
```math
R_0(1-p_c)=1\qquad \iff \qquad p_c=1-\frac{1}{R_0}
```
| Disease   | Transmission             | R0                     |
| ---       | ------                   | ---------:             |
| Measles   | Airborne	         | 	92–95%         |
| Pertussis | Airborne droplet         | 		92–94% |
| Mumps     | Airborne droplet	 | 75–86%                 |
| SARS      | Airborne droplet	 | 50–80%                 |
| COVID-19  | Airborne droplet         | 29–74%                 |
| Ebola     | fluids                   | 		33–60% |
| Influenza | Airborne droplet	 | 	33–44%         |  

[^1]
#### Why we should not trust $`R_0`$
Includes an exposed compartement in order to model the incubation time, when people seem
suceptible but are in reality already infectious.
### The SEIR model
<p align="right">
  <img src="../../figures/seir.png">
</p>

#### Equations 
```math
\begin{aligned}
\frac{dS}{dt}&=-\beta I S\\
\frac{dE}{dt}&=\beta I S-\sigma E\\
\frac{dI}{dt}&=\sigma I -\gamma I\\
\frac{dR}{dt}&=\gamma I\\
N&\approx S+I+R
\end{aligned}
```
#### Parameters
$`\sigma`$:	controls how long the incubation phase lasts.  

# Network Models
Simple mathematical models of epidemics assume random mixing between individuals, which is clearly not the case in reality. Populations have structure, and individuals tend to interact with a very limited subset of the population. Contacts or links between the individuals constitute a network, and infectious diseases that propagate by direct contact can only spread along the paths of these networks. [^5]

# Agent Based Models

### Literature and Links
#### 
[Theoretical Biology -ETH](https://tb.ethz.ch/education/learningmaterials/modelingcourse/level-1-modules.html)  

# Machine Learning approaches

### Library and Frameworks
[Mesa - Agent Base Modelin in Python](https://mesa.readthedocs.io/en/master/)
[Epidemic On Networks [EoN]](https://epidemicsonnetworks.readthedocs.io/en/latest/)

##
[^1]: [Wikipedia](https://en.wikipedia.org/wiki/Basic_reproduction_number).
[^2]: https://royalsocietypublishing.org/doi/10.1098/rspa.1927.0118
[^3]: https://arxiv.org/abs/1812.09759
[^4]: https://arxiv.org/abs/1403.2160
[^5]: https://tb.ethz.ch/education/learningmaterials/modelingcourse/level-2-modules/network.html
