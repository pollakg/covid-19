import pandas as pd
import numpy as np
from tqdm import tqdm
from models import simulate_model, get_model


def get_population_size(country_name):
    # TODO LOAD POPULATION SIZE FORM FILE
    assert country_name == 'Switzerland', "no data yet for other countries"
    return 8544500  # swiss population size


def load_ground_truth(country_name):
    """
    Load ground truth data for I and R
    :param country_name: the name of the country to load
    :return: I, R
    """
    with open('../../../data/Datalinks.txt', 'r') as f:
        linkfile = f.read().splitlines()
        cases_link = linkfile[1]
        recovery_link = linkfile[5]

    casesData = pd.read_csv(cases_link)
    casesData = casesData[casesData['Country/Region'] == country_name]
    recoveryData = pd.read_csv(recovery_link)
    recoveryData = recoveryData[recoveryData['Country/Region'] == country_name]

    groundtruth_I = []
    groundtruth_R = []

    for i in range(3, np.shape(recoveryData)[1]):  # we start from 4 because first columns are lat, long, etc..
        infected = casesData.iloc[:, i].values[0]
        if infected == 0 and len(groundtruth_I) == 0:
            continue  # we start our simulation from the first official case

        groundtruth_I.append(infected)
        groundtruth_R.append(recoveryData.iloc[:, i].values[0])

    return np.array(groundtruth_I, dtype=np.float32), np.array(groundtruth_R, dtype=np.float32)


def quadratic_error_function(I, R, Ihat, Rhat):
    """
    measure the error between I R and Ihat Rhat
    :param I:
    :param R:
    :param Ihat:
    :param Rhat:
    :return:
    """
    error_I = ((I - Ihat) ** 2).mean()
    error_R = ((R - Rhat) ** 2).mean()

    return error_I + error_R


if __name__ == '__main__':
    # TODO: make the following as arguments of the source code
    country = 'Switzerland'
    model_name = 'seir'

    # define parameter grid to search through currently all is defined only by sigma and r0
    # this could and should be changed
    print('Set up search grid..')
    sigmas = [1/(0.1*x) for x in range(1, 24)]  # TODO find a reasonalbe range to try here
    r0s = [0.1*x for x in range(2, 24)]

    grid_search_params = []
    for r0 in r0s:
        for sigma in sigmas:
            generationTime = 4.6  # https://www.medrxiv.org/content/10.1101/2020.03.05.20031815v1  http://www.cidrap.umn.edu/news-perspective/2020/03/short-time-between-serial-covid-19-cases-may-hinder-containment
            gamma = 1.0 / (2.0 * (
                        generationTime - 1.0 / sigma))
            beta = sigma * r0
            grid_search_params.append((beta, gamma, sigma))

    # load data:
    print('Load data..')
    population = get_population_size(country)
    Igt, Rgt = load_ground_truth(country)
    E0 = Igt[0]

    # run gridsearch
    print('{} configurations to search through..'.format(len(grid_search_params)))
    best_params = []
    best_run = float("inf")
    for params in tqdm(grid_search_params):
        # simulate
        model = get_model(model_name)
        S, E, I, R = simulate_model(model, population, E0, len(Igt), *params)

        # evaluate
        error = quadratic_error_function(Igt, Rgt, I, R)
        if error < best_run:
            best_params = params
            best_run = error

    print('grid search finished.. params {} with an error of {}'.format(best_params, best_run))