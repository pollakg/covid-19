import numpy as np
import scipy.integrate


def simulate_model(model_fn, N0, E0, sim_time, *args):
    t = np.arange(sim_time)  # time steps array
    params0 = N0-E0, E0, 0, 0  # S E I R initially

    y_data_var = scipy.integrate.odeint(model_fn, params0, t, args=(N0, *args))

    return y_data_var.T


# TODO: this is a little ugly maybe make as abstract class later..
def get_model(name):
    if name == 'seir':
        return seir_model
    if name == 'seir_intervention':
        return seir_model_intervention
    raise ValueError('Unknown model')


def seir_model(old_vals, t, N, beta, gamma, sigma):
    """
    :param N: population size
    :param beta: controlong how often suceptilbe-infected contact results in new exposure
    :param gamma: rate an infected recovers and moves into the resistant phase
    :param sigma: rate at which exposed person becomes infective
    :return:
    """

    S, E, I, R = old_vals

    dS = - beta * S * I / N
    dE = beta * S * I / N - sigma * E
    dI = sigma * E - gamma * I
    dR = gamma * I
    return dS, dE, dI, dR


def seir_model_intervention(old_vals, t, N, beta_plain, beta_intervention, intervention_day, gamma, sigma):
    """
    :param N: population size
    :param beta_plain: controlong how often suceptilbe-infected contact results in new exposure
    :param beta_intervention: same as beta_plain but after the lockdown measures have been applied
    :param intervention_day: day (in measures of t) when lockdown was applied
    :param gamma: rate an infected recovers and moves into the resistant phase
    :param sigma: rate at which exposed person becomes infective
    :return:
    """

    beta = beta_plain if t < intervention_day else beta_intervention

    S, E, I, R = old_vals

    dS = - beta * S * I / N
    dE = beta * S * I / N - sigma * E
    dI = sigma * E - gamma * I
    dR = gamma * I
    return dS, dE, dI, dR
