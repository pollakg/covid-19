import requests
import json
from pathlib import Path
import shelve
import datetime
import pandas as pd
if __name__ == "__main__":
    from dw_config import base_url, api_token, db_name
else:
    from .dw_config import base_url, api_token, db_name

base_path = str(Path(__file__).parent)

def shelve_data(database,data):
    print ("database name: ", database)
    my_shelve = shelve.open(base_path + "/" + db_name)
    input_data = {
        "data":data,
        "last_day_updated":datetime.datetime.now().date()
    }
    my_shelve[database]=input_data
    my_shelve.close()

def deshelve(database):
    db = shelve.open(base_path + "/" + db_name)
    date = db[database]["last_day_updated"]
    data = db[database]["data"]
    db.close()
    outdict = {
        "date": date,
         "data": data
    }
    return outdict


def get_data(username,id,database_name):
    deshelved_data = deshelve(database_name)
    if deshelved_data["date"] == datetime.datetime.now().date():
        print("deshelving locally")
        return deshelved_data["data"]
    else:
        print("pulling data from internet")
        url = base_url + "sql/" + username + "/" + id
        headers = {
            "Authorization": "Bearer " + api_token
        }
        data = {
            "query": "SELECT * FROM " + database_name
        }
        response = requests.post(url,headers=headers,data=data)
        response_json = response.json()
        shelve_data(database_name,response_json)
        return response_json

def list_database(username,id):
    url = base_url + "datasets/" + username + "/" + id
    # print(url)
    headers = {
        "Authorization": "Bearer " + api_token,
        "Content-type": "application/json"
    }
    response = requests.get(url,headers=headers)
    return response.json()["files"]

def json_to_pd_gov_restrictions(data):
    answer_dict = {
        "a": 1000,
        "b": 500,
        "c": 250,
        "d": 50,
        "e": 10,
        "f": 0
    }
    data_dict = {
        "country": [],
        "location_wiki":[],
        "question":[],
        "max_gathering_size": [],
        "date":[]
    }
    for i in data:
        data_dict["country"].append(i["country"])
        data_dict["location_wiki"].append(i["location_wiki"])
        data_dict["question"].append(i["question"])
        data_dict["max_gathering_size"].append(answer_dict[ i["answer"] ])
        data_dict["date"].append(i["date"])

    return pd.DataFrame.from_dict(data_dict)

def get_df(data):
    df=json_to_pd_gov_restrictions(data)
    print(df)
    return df


if __name__ == "__main__":
    database_list = list_database("liz-friedman","crowd-sourced-covid-19-government-restrictions")
    # for database in database_list:
    #     print(database["name"])
    database = database_list[0]["name"][:-4]
    print (database)
    my_json = get_data("liz-friedman","crowd-sourced-covid-19-government-restrictions",database)
    print (json_to_pd_gov_restrictions(my_json))
    # print (my_json)
    # my_json_formated = json.dumps(my_json[0],indent = 2)
    # for dict in my_json:
    #     json_formated = json.dumps(dict, indent=2)
    #     print (json_formated)