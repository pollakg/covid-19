# Datasets

## Global dataset
This dataset contains timeseries and metadata for most countries of the world.
### structure of the returned data
The data module returns a dictionary with the countries we wanted to get data for. 
Each country has two datastructures callled "MetaData" and "TimeSeries". 

The **Metadata** is a Pandas DataFrame with the following entries: 
- Population
- Density
- Land Area
- Med. Age
- Urban Population (percent)

The **TimeSeries** field contains a pandas data frame whose structure can be seen in the following table:


index |stringency | stringency_actual | deaths | confirmed | S1'*' | S2'*' | S3'*' | S4'*' | S5'*' | S6'*' | S7 | S12 | S13
--- | --- | --- | --- |--- |--- |--- |--- |--- |--- |--- |---|--- |---
2020-01-01 |  |  |  |  |  |  |  |  |  |  | |  | 
... |  |  |  | | | | | | | || |
2020-04-13 |  |  |  |  |  |  |  |  |  |  | |  | 

'*': *each of this columns actually consists of two columns which are here put together for clarity: SX_policy_value, SX_isgeneral*
For information about the columns please refer to the next section

### About the source
- Metadata: The metadata is taken from [worldometer](https://www.worldometers.info/world-population/population-by-country/) and their meaning should be rather straightforward.
- TimeSeries:The TimeSeries are taken from [Oxford Government Response Tracker](https://www.bsg.ox.ac.uk/research/research-projects/oxford-covid-19-government-response-tracker).
 They defined 13 indicators of government indicators:
    1. school closure;
    2. workplace closures;
    3. public event cancellation;
    4. public transport closure;
    5. public information campaigns;
    6. restriction on internal movement;
    7. international travel controls;
    8. fiscal measures;
    9. monetary measures;
    10. emergency investment in healthcare;
    11. investment in vaccines.
    12. testing framework
    13. contact tracing
For each of indicators they defined a *policyvalue* according to a coding scheme which can be found [here](https://www.bsg.ox.ac.uk/sites/default/files/2020-04/BSG-WP-2020-031-v4.0_0.pdf)
Indicators 1-6 are further classified either as  “targeted” (meaning they apply only in a geographically concentrated area) or “general” (meaning they apply throughout the entire jurisdiction) which relates to the field *is_general*.
From these indicators they then calculated a [stringency index](https://www.bsg.ox.ac.uk/sites/default/files/Calculation%20and%20presentation%20of%20the%20Stringency%20Index.pdf) for each country and day which is in the range of 0-100.

### How to
As pulling the dataset is extremely time consuming (we have to do a request for each day and country),  the already downloaded is saved in a shelve (oxford_db) and only once a day the new data is then added.
To get the data out of the shelve and pulling new data from the internet, the following call is needed. 
```
data= global_dataset.get_data('oxford')
```
The data can then be converted into the datastructure explained below (list of country dictionaries) with the following call.
```
list_of_countries=['CHE','ITA','DEU','CHN']
df= global_dataset.get_df(data,list_of_countries)
```
The countries need to have the 3-digit country code according to the ISO standard. It is highly recommended not to query for all countries (no list as argument) as this is memory consuming.

Exemplary structure of the returned data:
```
df=[{'CHE':
            {"TimeSeries":Pandas.DataFrame,
            "MetaData": Pandas.DataFame}
        },
    ...,
    {'CHN':
            {"TimeSeries":Pandas.DataFrame,
            "MetaData": Pandas.DataFame}
        }
    ]

```
## US States dataset
To be done by Hei Kern
## Further Data
### Regional Data
#### Italy	
- https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni.csv

#### Spain	
- https://github.com/datadista/datasets/tree/master/COVID%2019

#### United States	
- https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv

#### Canada	
- https://github.com/CSSEGISandData/COVID-19

#### Switzerland	
- https://raw.githubusercontent.com/openZH/covid_19/master/COVID19_Fallzahlen_CH_total.csv
