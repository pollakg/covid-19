import requests
import shelve
from config import field_list, url, filepath

def get_states(file):
    state_list = []
    with open(file) as file:
        states = file.readlines()
        for state in states:
            state_list.append(state[:-1])
    return state_list

def get_state_dict(state_list):
    state_dict = {}
    for state in state_list:
        state_dict[state] = {
            "state_name": state,
            "state_data": {}
        }
    return state_dict

def data_processor(data):
    return (0 if data == None else data)

def api_request(state):
    params = {
        "state" : state
    }
    response = requests.get(url,params)
    return response.json()

def data_select(state, fields):
    data_dict = {}
    req_data = api_request(state)
    for field in fields:
        data_dict[field] = []
        for day in req_data:
            day_data = data_processor(day.get(field,0))
            data_dict[field].append(day_data)
        data_dict[field].reverse()
    return data_dict


def fetch_data(field_list):
    state_list = get_states("states.txt")
    state_dict = get_state_dict(state_list)
    for state in state_list:
        state_dict[state]["state_data"] = data_select(state,field_list)
    return state_dict

def shelve_data(data):
    my_shelve = shelve.open(filepath)
    my_shelve["us_data"] = data
    my_shelve.close()


if __name__ == "__main__":
    data = fetch_data(field_list)
    shelve_data(data)

